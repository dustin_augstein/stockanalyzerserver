#!/bin/bash
sed -i 's/PLACEHOLDER_SERVER_URL/'$REACT_APP_SERVER_URL':'$REACT_APP_SERVER_PORT'/g' /BOOT-INF/classes/public/config.js
sed -i 's/PLACEHOLDER_OAUTHSERVER_URL/'$REACT_OAUTH_URL':'$REACT_OAUTH_PORT'/g' /BOOT-INF/classes/public/config.js
sed -i 's/PLACEHOLDER_SCHEMA/'$REACT_APP_SCHEMA'/g' /BOOT-INF/classes/public/config.js
sed -i 's/PLACEHOLDER_PL_SERVER_URL/'$REACT_APP_PL_SERVER_URL'/g' /BOOT-INF/classes/public/config.js

echo REACT_APP_SERVER_URL:REACT_APP_SERVER_PORT = $REACT_APP_SERVER_URL : $REACT_APP_SERVER_PORT
echo REACT_OAUTH_URL:REACT_OAUTH_PORT = $REACT_OAUTH_URL : $REACT_OAUTH_PORT
echo REACT_APP_SCHEMA = $REACT_APP_SCHEMA
echo REACT_APP_PL_SERVER_URL = $REACT_APP_PL_SERVER_URL

jar uf /app.jar /BOOT-INF/classes/public/config.js
java -Djava.security.egd=file:/dev/./urandom -jar /app.jar
