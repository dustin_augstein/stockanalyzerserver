package com.stockanalyzer;

import java.io.IOException;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;

import com.stockanalyzer.service.ServiceLayer;

@SpringBootApplication
@ComponentScan(basePackages = {"com.stockanalyzer"})
@PropertySource(value = "classpath:application.yml")
public class Application 
{
	@Autowired
	private ServiceLayer service;
	
	public static void main(String[] args) 
	{
		SpringApplication.run(Application.class, args);
	}
	
	@PostConstruct
	public void startDataOptainer() throws IOException
	{
		this.service.startDataOptainer();
	}
}