package com.stockanalyzer;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.core.Ordered;

@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class SimpleCorsFilter implements Filter
{
	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException
	{
		HttpServletRequest request = (HttpServletRequest) req;
		if ("OPTIONS".equalsIgnoreCase(request.getMethod()))
		{
			HttpServletResponse response = (HttpServletResponse) res;
			response.setHeader("Access-Control-Allow-Origin", "*");
			response.setHeader("Access-Control-Allow-Methods", "POST, PUT, PATCH, DELETE");
			response.setHeader("Access-Control-Allow-Headers", "authorization, content-type, WebSocket-SessionID, Referer");
			response.setStatus(HttpServletResponse.SC_OK);
		} else
		{
			HttpServletResponse response = (HttpServletResponse) res;
			response.setHeader("Access-Control-Allow-Origin", "*");
			chain.doFilter(req, res);
		}
	}

	@Override
	public void init(FilterConfig filterConfig)
	{
	}

	@Override
	public void destroy()
	{
	}
}