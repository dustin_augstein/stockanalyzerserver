package com.stockanalyzer.reflective_translator;

import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeansException;
import org.springframework.beans.FatalBeanException;
import org.springframework.lang.Nullable;
import org.springframework.util.Assert;
import org.springframework.util.ClassUtils;

public class ReflectiveTranslator
{
	public static <T> Collection<T> translate(Class<T> targetClass, Iterable<?> sourceList)
	{
		if (sourceList == null)
			return null;

		Collection<T> result = new ArrayList<T>();
		for (Object source : sourceList)
			result.add(translate(targetClass, source));
		return result;
	}

	public static <T> T translate(Class<T> targetClass, Object... sources)
	{
		if (sources == null)
			return null;

		if (sources.length == 1 && targetClass.isEnum())
		{
			return findEnum(targetClass.getEnumConstants(), sources[0]);
		} else
		{
			T target = BeanUtils.instantiateClass(targetClass);
			translate(target, sources);
			return target;
		}
	}

	private static <T, S> T findEnum(T[] source, S name)
	{
		for (T enumEntry : source)
		{
			if (enumEntry.toString().equals(name.toString()))
				return enumEntry;
		}
		return null;
	}

	public static <T> T translate(T target, Object... sources)
	{
		if (sources == null || sources.length == 0 || sources[0] == null)
			return target;

		copyPrimitiveOrWrappedOrStringProperties(sources[0], target, null);

		for (Method method : target.getClass().getMethods())
		{
			if (method.getAnnotation(AdditionalTranslationsFrom.class) != null)
			{
				boolean match = false;
				Class<?>[] parameterTypes = method.getParameterTypes();
				Class<?>[] sourceTypes = getTypes(sources);
				if (parameterTypes.length == sourceTypes.length)
				{
					match = true;
					for (int i = 0; i < sourceTypes.length; i++)
					{
						if (!parameterTypes[i].isAssignableFrom(sourceTypes[i]))
						{
							match = false;
							break;
						}
					}
				}
				if (match)
					callMethod(target, method, sources);
			}
		}
		
		for (Object source : sources)
		{
			for (Method method : source.getClass().getMethods())
			{
				if (method.getAnnotation(AdditionalTranslationsTo.class) != null)
				{
					Class<?>[] parameterTypes = method.getParameterTypes();
					if (parameterTypes.length > 1)
						throw new RuntimeException("Translation methods, annoatated wird AdditionalTranslationsTo should only have one Parameter!");
					if (parameterTypes[0].isAssignableFrom(target.getClass()))
						callMethod(source, method, target);
				}
			}
		}
		
		return target;
	}

	private static Class<?>[] getTypes(Object... sources)
	{
		Class<?>[] result = new Class<?>[sources.length];
		for (int i = 0; i < sources.length; i++)
			result[i] = sources[i].getClass();
		return result;
	}

	/**
	 * Copy the property values of the given source bean into the given target bean.
	 * <p>
	 * Note: The source and target classes do not have to match or even be derived
	 * from each other, as long as the properties match. Any bean properties that
	 * the source bean exposes but the target bean does not will silently be
	 * ignored.
	 * 
	 * @param source           the source bean
	 * @param target           the target bean
	 * @param editable         the class (or interface) to restrict property setting
	 *                         to
	 * @param ignoreProperties array of property names to ignore
	 * @throws BeansException if the copying failed
	 * @see BeanWrapper
	 */
	private static void copyPrimitiveOrWrappedOrStringProperties(Object source, Object target, @Nullable Class<?> editable,
			@Nullable String... ignoreProperties) throws BeansException
	{
		if (source == null)
			return;

		Assert.notNull(target, "Target must not be null");

		Class<?> actualEditable = target.getClass();
		if (editable != null)
		{
			if (!editable.isInstance(target))
			{
				throw new IllegalArgumentException(
						"Target class [" + target.getClass().getName() + "] not assignable to Editable class [" + editable.getName() + "]");
			}
			actualEditable = editable;
		}
		PropertyDescriptor[] targetPds = BeanUtils.getPropertyDescriptors(actualEditable);
		List<String> ignoreList = (ignoreProperties != null ? Arrays.asList(ignoreProperties) : null);

		for (PropertyDescriptor targetPd : targetPds)
		{
			if (!isTransferableProperty(targetPd.getPropertyType()))
				continue;

			Method writeMethod = targetPd.getWriteMethod();
			if (writeMethod != null && (ignoreList == null || !ignoreList.contains(targetPd.getName())))
			{
				PropertyDescriptor sourcePd = BeanUtils.getPropertyDescriptor(source.getClass(), targetPd.getName());
				if (sourcePd != null)
				{
					Method readMethod = sourcePd.getReadMethod();
					if (readMethod != null)
					{
						if (ClassUtils.isAssignable(writeMethod.getParameterTypes()[0], readMethod.getReturnType()))
						{
							try
							{
								transferProperty(source, readMethod, target, writeMethod);
							} catch (Throwable ex)
							{
								throw new FatalBeanException("Could not copy property '" + targetPd.getName() + "' from source to target", ex);
							}
						} else if (targetPd.getPropertyType().isEnum() && sourcePd.getPropertyType().isEnum())
						{
							try
							{
								transferEnumProperty(source, readMethod, target, writeMethod, targetPd.getPropertyType());
							} catch (Throwable ex)
							{
								throw new FatalBeanException("Could not copy property '" + targetPd.getName() + "' from source to target", ex);
							}
						}
					}
				}
			}
		}
	}

	private static void transferProperty(Object source, Method readMethod, Object target, Method writeMethod)
	{
		Object value = callMethod(source, readMethod);
		callMethod(target, writeMethod, value);
	}

	private static void transferEnumProperty(Object source, Method readMethod, Object target, Method writeMethod, Class<?> targetType)
	{
		Object value = callMethod(source, readMethod);
		callMethod(target, writeMethod, translate(targetType, value));
	}

	private static Object callMethod(Object target, Method method, Object... parameters)
	{
		if (!Modifier.isPublic(method.getDeclaringClass().getModifiers()))
		{
			method.setAccessible(true);
		}
		try
		{
			return method.invoke(target, parameters);
		} catch (IllegalAccessException e)
		{
			e.printStackTrace();
		} catch (IllegalArgumentException e)
		{
			e.printStackTrace();
		} catch (InvocationTargetException e)
		{
			e.printStackTrace();
		}
		return null;
	}

	private static boolean isTransferableProperty(Class<?> clazz)
	{
		return ClassUtils.isPrimitiveOrWrapper(clazz) || clazz == String.class || clazz == Date.class || clazz.isEnum();
	}
}
