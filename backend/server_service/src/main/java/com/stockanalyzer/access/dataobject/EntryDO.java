package com.stockanalyzer.access.dataobject;

import javax.persistence.Entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(callSuper = false)
public class EntryDO extends AbstractDO 
{
	private String timestamp;
	private String ticker;
	private String reddit;
	private String prevScore;
	private String scoreChange;
	private String rockets;
	private String stockPrice;
	private String prevClose;
	private String change;
	private String shorted;
	private String volume;
	
	private String type;
	private String interval;
	
	public EntryDO()
	{
		super();
	}
	
	public EntryDO(String id)
	{
		super(id);
	}
}
