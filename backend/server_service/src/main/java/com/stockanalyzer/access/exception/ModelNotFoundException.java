package com.stockanalyzer.access.exception;

public class ModelNotFoundException extends RuntimeException
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -4591310989075979496L;

	public ModelNotFoundException(Class<?> clazz,  String id)
	{
		super("Model of Type: \"" + clazz.getSimpleName() + "\" and id: \"" + id + "\" is unknown.");
	}
}
