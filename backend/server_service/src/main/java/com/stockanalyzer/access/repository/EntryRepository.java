package com.stockanalyzer.access.repository;

import org.springframework.stereotype.Repository;

import com.stockanalyzer.access.dataobject.EntryDO;

@Repository
public interface EntryRepository extends IBaseRepository<EntryDO>
{
}
