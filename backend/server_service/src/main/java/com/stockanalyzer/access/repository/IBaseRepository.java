package com.stockanalyzer.access.repository;

import java.util.Collection;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface IBaseRepository<O> extends MongoRepository<O, String>
{
	@Query("{_id: { $in: ?0 } }")
    Collection<O> findByIds(Collection<String> ids);
}
