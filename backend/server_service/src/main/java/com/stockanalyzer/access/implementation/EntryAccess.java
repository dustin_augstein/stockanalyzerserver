package com.stockanalyzer.access.implementation;

import org.springframework.stereotype.Component;

import com.stockanalyzer.access.dataobject.EntryDO;
import com.stockanalyzer.access.repository.EntryRepository;
import com.stockanalyzer.service.model.EntryModel;

@Component
public class EntryAccess extends AbstractGenericAccess<EntryModel, EntryDO, EntryRepository>
{
	public EntryAccess()
	{
		super(EntryModel.class, EntryDO.class);
	}
}
