package com.stockanalyzer.access.implementation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Query;

import com.stockanalyzer.access.dataobject.AbstractDO;
import com.stockanalyzer.access.exception.ModelNotFoundException;
import com.stockanalyzer.access.repository.IBaseRepository;
import com.stockanalyzer.reflective_translator.ReflectiveTranslator;
import com.stockanalyzer.service.model.AbstractModel;


public abstract class AbstractGenericAccess<T extends AbstractModel, O extends AbstractDO, R extends IBaseRepository<O>> extends AbstractAccess<T, O, R>
{
	private Class<T> modelClass;
	private Class<O> dataObjectClass;
	
	@Autowired
	private MongoOperations mongoOps;

	public AbstractGenericAccess(Class<T> modelClass, Class<O> dataObjectClass)
	{
		this.modelClass = modelClass;
		this.dataObjectClass = dataObjectClass;
	}

	@Override
	public T create(T model)
	{
		O dataObject = ReflectiveTranslator.<O>translate(this.dataObjectClass, model);
		dataObject = this.repository.save(dataObject);
		return ReflectiveTranslator.translate(this.modelClass, dataObject);
	}

	@Override
	public T update(T model)
	{
		if (model.getId() == null)
			throw new RuntimeException(getName(model) + " has no id. Please save it before trying to update it.");
		if (!this.repository.existsById(model.getId()))
			throw new RuntimeException(getName(model) + " with id: \"" + model.getId() + "\" is unknown, so it can't get updated.");
		O companyDO = this.repository.save(ReflectiveTranslator.<O>translate(this.dataObjectClass, model));
		return ReflectiveTranslator.<T>translate(this.modelClass, companyDO);
	}

	@Override
	public T findById(String id)
	{
		Optional<O> optionalDataObject = this.repository.findById(id);
		if (!optionalDataObject.isPresent())
			throw new ModelNotFoundException(this.dataObjectClass, id);
		return ReflectiveTranslator.<T>translate(this.modelClass, optionalDataObject.get());
	}

	public Collection<T> findByIds(Set<String> ids)
	{
		Collection<O> optionalDataObject = this.repository.findByIds(ids);
		return ReflectiveTranslator.<T>translate(this.modelClass, optionalDataObject);
	}

	@Override
	public Collection<T> findAll()
	{
		List<O> pagedDataObjects = this.repository.findAll();
		return ReflectiveTranslator.<T>translate(this.modelClass, pagedDataObjects);
	}

	public Collection<T> findPaged(int page, int pageSize)
	{
		Page<O> pagedDataObjects = this.repository.findAll(PageRequest.of(page, pageSize, Sort.by("id")));
		return ReflectiveTranslator.<T>translate(this.modelClass, pagedDataObjects);
	}

	public void deleteAll()
	{
		this.repository.deleteAll();
	}

	public void deleteById(String id)
	{
		this.repository.deleteById(id);
	}

	/**
	 * @param search -> https://github.com/RutledgePaulV/rest-query-engine
	 * @return
	 */
	public Collection<T> search(Query query)
	{
		List<O> temp = mongoOps.find(query, this.dataObjectClass);
		return ReflectiveTranslator.<T>translate(this.modelClass, temp);
	}

	/**
	 * @param search -> https://github.com/RutledgePaulV/rest-query-engine
	 * @return
	 */
	public Page<T> search(Query search, Integer page, Integer size)
	{
		List<T> temp = new ArrayList<T>(search(search));
		Pageable pageable = PageRequest.of(page, size);
		return new PageImpl<T>(temp, pageable, temp.size());
	}

	public boolean existsById(String id)
	{
		return this.repository.existsById(id);
	}

	private static String getName(AbstractModel model)
	{
		return model.getClass().getSimpleName();
	}
}
