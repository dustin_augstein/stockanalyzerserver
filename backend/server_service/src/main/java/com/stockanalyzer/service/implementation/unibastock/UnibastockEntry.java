package com.stockanalyzer.service.implementation.unibastock;

import lombok.Data;

@Data
public class UnibastockEntry
{
	private long interval;
	private String type;
	private String unibastockInterval;

	public UnibastockEntry(long interval, String type, String unibastockInterval)
	{
		this.interval = interval;
		this.type = type;
		this.unibastockInterval = unibastockInterval;
	}
}
