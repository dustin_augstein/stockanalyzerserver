package com.stockanalyzer.service;

import java.io.IOException;

public interface ServiceLayer
{
	public void startDataOptainer() throws IOException;
}
