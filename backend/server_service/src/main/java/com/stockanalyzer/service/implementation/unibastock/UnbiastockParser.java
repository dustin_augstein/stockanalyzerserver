package com.stockanalyzer.service.implementation.unibastock;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.stockanalyzer.service.model.EntryModel;

public class UnbiastockParser
{
	private static String BASE_URL = "https://unbiastock.com/TableReddit.php";
	private static String PARAMS_COMPARE = "?compare2=";
	private static String PARAMS_COMPARE_SECTOR = "&compare_sector=";
	private static String PARAMS_MAIL_NEWS = "&mailnews=";
	
	private static SimpleDateFormat SDF = new SimpleDateFormat("dd.MM HH:mm:ss");

	public List<EntryModel> getList(String type, String interval) throws IOException
	{
		String url = BASE_URL + PARAMS_COMPARE + type + PARAMS_COMPARE_SECTOR + interval + PARAMS_MAIL_NEWS;
		Document doc;
		doc = Jsoup.connect(url).get();
		Elements nodes = doc.select("table").first().select("tbody").first().select("tr");
		List<EntryModel> result = new ArrayList<EntryModel>();
		String timestamp = SDF.format(new Date());
		if (nodes != null)
			nodes.forEach((e) -> { result.add(parseEntry(timestamp, e, type, interval)); });
		return result;
	}

	private EntryModel parseEntry(String timestamp, Element element, String type, String interval)
	{
		EntryModel result = new EntryModel();
		result.setTimestamp(timestamp);
		result.setTicker(element.child(0).child(0).html());
		result.setTickerHref(element.child(0).child(0).attr("href"));
		result.setReddit((element.child(1).html()));
		result.setPrevScore(element.child(2).html());
		result.setScoreChange(element.child(3).html());
		result.setRockets(element.child(4).html());
		result.setStockPrice(element.child(5).html());
		result.setPrevClose(element.child(6).html());
		result.setChange(element.child(7).html());
		result.setShorted(element.child(8).html());
		result.setVolume(element.child(9).html());
		result.setType(type);
		result.setInterval(interval);
		return result;
	}
}
