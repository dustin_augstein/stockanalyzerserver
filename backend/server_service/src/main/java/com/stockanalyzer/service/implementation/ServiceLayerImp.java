package com.stockanalyzer.service.implementation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stockanalyzer.access.implementation.EntryAccess;
import com.stockanalyzer.service.ServiceLayer;
import com.stockanalyzer.service.implementation.unibastock.UnibastockEntry;
import com.stockanalyzer.service.implementation.unibastock.UnbiastockParser;
import com.stockanalyzer.service.model.EntryModel;

import static com.stockanalyzer.service.Helper.quote;

@Service
public class ServiceLayerImp implements ServiceLayer
{
	private static Logger logger = LoggerFactory.getLogger(ServiceLayerImp.class);

	private static long INTERVAL = 1000 * 60 * 60 * 6;

	private static List<UnibastockEntry> UNIBASTOCK_ENTRIES = new ArrayList<UnibastockEntry>();
	static
	{
		
		UNIBASTOCK_ENTRIES.add(new UnibastockEntry(INTERVAL, "wallstreetbets", "6"));
		UNIBASTOCK_ENTRIES.add(new UnibastockEntry(INTERVAL, "StockMarket", "6"));
		UNIBASTOCK_ENTRIES.add(new UnibastockEntry(INTERVAL, "pennystocks", "6"));
		UNIBASTOCK_ENTRIES.add(new UnibastockEntry(INTERVAL, "investing", "6"));
		UNIBASTOCK_ENTRIES.add(new UnibastockEntry(INTERVAL, "stocks", "6"));
		UNIBASTOCK_ENTRIES.add(new UnibastockEntry(INTERVAL, "RobinHoodPennyStocks", "6"));
		UNIBASTOCK_ENTRIES.add(new UnibastockEntry(INTERVAL, "Daytrading", "6"));
		UNIBASTOCK_ENTRIES.add(new UnibastockEntry(INTERVAL, "SPACs", "6"));
		UNIBASTOCK_ENTRIES.add(new UnibastockEntry(INTERVAL, "weedstocks", "6"));
		UNIBASTOCK_ENTRIES.add(new UnibastockEntry(INTERVAL, "CanadianInvestor", "6"));
		UNIBASTOCK_ENTRIES.add(new UnibastockEntry(INTERVAL, "smallstreetbets", "6"));
		
		UNIBASTOCK_ENTRIES.add(new UnibastockEntry(INTERVAL, "wallstreetbets", "12"));
		UNIBASTOCK_ENTRIES.add(new UnibastockEntry(INTERVAL, "StockMarket", "12"));
		UNIBASTOCK_ENTRIES.add(new UnibastockEntry(INTERVAL, "pennystocks", "12"));
		UNIBASTOCK_ENTRIES.add(new UnibastockEntry(INTERVAL, "investing", "12"));
		UNIBASTOCK_ENTRIES.add(new UnibastockEntry(INTERVAL, "stocks", "12"));
		UNIBASTOCK_ENTRIES.add(new UnibastockEntry(INTERVAL, "RobinHoodPennyStocks", "12"));
		UNIBASTOCK_ENTRIES.add(new UnibastockEntry(INTERVAL, "Daytrading", "12"));
		UNIBASTOCK_ENTRIES.add(new UnibastockEntry(INTERVAL, "SPACs", "12"));
		UNIBASTOCK_ENTRIES.add(new UnibastockEntry(INTERVAL, "weedstocks", "12"));
		UNIBASTOCK_ENTRIES.add(new UnibastockEntry(INTERVAL, "CanadianInvestor", "12"));
		UNIBASTOCK_ENTRIES.add(new UnibastockEntry(INTERVAL, "smallstreetbets", "12"));
		
		UNIBASTOCK_ENTRIES.add(new UnibastockEntry(INTERVAL, "wallstreetbets", "24"));
		UNIBASTOCK_ENTRIES.add(new UnibastockEntry(INTERVAL, "StockMarket", "24"));
		UNIBASTOCK_ENTRIES.add(new UnibastockEntry(INTERVAL, "pennystocks", "24"));
		UNIBASTOCK_ENTRIES.add(new UnibastockEntry(INTERVAL, "investing", "24"));
		UNIBASTOCK_ENTRIES.add(new UnibastockEntry(INTERVAL, "stocks", "24"));
		UNIBASTOCK_ENTRIES.add(new UnibastockEntry(INTERVAL, "RobinHoodPennyStocks", "24"));
		UNIBASTOCK_ENTRIES.add(new UnibastockEntry(INTERVAL, "Daytrading", "24"));
		UNIBASTOCK_ENTRIES.add(new UnibastockEntry(INTERVAL, "SPACs", "24"));
		UNIBASTOCK_ENTRIES.add(new UnibastockEntry(INTERVAL, "weedstocks", "24"));
		UNIBASTOCK_ENTRIES.add(new UnibastockEntry(INTERVAL, "CanadianInvestor", "24"));
		UNIBASTOCK_ENTRIES.add(new UnibastockEntry(INTERVAL, "smallstreetbets", "24"));
		
		UNIBASTOCK_ENTRIES.add(new UnibastockEntry(INTERVAL, "wallstreetbets", "48"));
		UNIBASTOCK_ENTRIES.add(new UnibastockEntry(INTERVAL, "StockMarket", "48"));
		UNIBASTOCK_ENTRIES.add(new UnibastockEntry(INTERVAL, "pennystocks", "48"));
		UNIBASTOCK_ENTRIES.add(new UnibastockEntry(INTERVAL, "investing", "48"));
		UNIBASTOCK_ENTRIES.add(new UnibastockEntry(INTERVAL, "stocks", "48"));
		UNIBASTOCK_ENTRIES.add(new UnibastockEntry(INTERVAL, "RobinHoodPennyStocks", "48"));
		UNIBASTOCK_ENTRIES.add(new UnibastockEntry(INTERVAL, "Daytrading", "48"));
		UNIBASTOCK_ENTRIES.add(new UnibastockEntry(INTERVAL, "SPACs", "48"));
		UNIBASTOCK_ENTRIES.add(new UnibastockEntry(INTERVAL, "weedstocks", "48"));
		UNIBASTOCK_ENTRIES.add(new UnibastockEntry(INTERVAL, "CanadianInvestor", "48"));
		UNIBASTOCK_ENTRIES.add(new UnibastockEntry(INTERVAL, "smallstreetbets", "48"));
		
		UNIBASTOCK_ENTRIES.add(new UnibastockEntry(INTERVAL, "wallstreetbets", "168"));
		UNIBASTOCK_ENTRIES.add(new UnibastockEntry(INTERVAL, "StockMarket", "168"));
		UNIBASTOCK_ENTRIES.add(new UnibastockEntry(INTERVAL, "pennystocks", "168"));
		UNIBASTOCK_ENTRIES.add(new UnibastockEntry(INTERVAL, "investing", "168"));
		UNIBASTOCK_ENTRIES.add(new UnibastockEntry(INTERVAL, "stocks", "168"));
		UNIBASTOCK_ENTRIES.add(new UnibastockEntry(INTERVAL, "RobinHoodPennyStocks", "168"));
		UNIBASTOCK_ENTRIES.add(new UnibastockEntry(INTERVAL, "Daytrading", "168"));
		UNIBASTOCK_ENTRIES.add(new UnibastockEntry(INTERVAL, "SPACs", "168"));
		UNIBASTOCK_ENTRIES.add(new UnibastockEntry(INTERVAL, "weedstocks", "168"));
		UNIBASTOCK_ENTRIES.add(new UnibastockEntry(INTERVAL, "CanadianInvestor", "168"));
		UNIBASTOCK_ENTRIES.add(new UnibastockEntry(INTERVAL, "smallstreetbets", "168"));
	}

	private UnbiastockParser unbiastockParser = new UnbiastockParser();

	@Autowired
	private EntryAccess entryAccess;

	@Override
	public void startDataOptainer() throws IOException
	{
		Timer t = new Timer();
		for (UnibastockEntry unibastockEntry : UNIBASTOCK_ENTRIES)
		{
			String type = unibastockEntry.getType();
			String unibastockInterval = unibastockEntry.getUnibastockInterval();
			long interval = unibastockEntry.getInterval();
			logger.info("Start UnibastockParserTask: " + quote(type) + " - " + quote(unibastockInterval) + " every " + interval + "ms");
			t.scheduleAtFixedRate(new ParserTask(this.unbiastockParser, this.entryAccess, type, unibastockInterval), 0, interval);
		}
	}

	private static class ParserTask extends TimerTask
	{
		private String type;
		private String interval;
		private EntryAccess entryAccess;
		private UnbiastockParser parser;

		public ParserTask(UnbiastockParser parser, EntryAccess entryAccess, String type, String interval)
		{
			this.parser = parser;
			this.entryAccess = entryAccess;
			this.type = type;
			this.interval = interval;
		}

		@Override
		public void run()
		{
			try
			{
				List<EntryModel> entryModels = this.parser.getList(type, interval);
				for (EntryModel entryModel : entryModels)
					this.entryAccess.create(entryModel);
				logger.info("Found " + entryModels.size() + " Entries with type: \"" + this.type + "\" and interval: \"" + this.interval + "\"");
			} catch (Exception e)
			{
				logger.error("Error while parsing entries of type:\"" + this.type + "\" interval: \"" + this.interval + "\"", e);
			}
		}
	}
}
