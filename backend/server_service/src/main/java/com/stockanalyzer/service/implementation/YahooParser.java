package com.stockanalyzer.service.implementation;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class YahooParser
{
	public void getStocks() throws IOException, InterruptedException
	{
		HttpRequest request = HttpRequest.newBuilder()
				.uri(URI.create("https://apidojo-yahoo-finance-v1.p.rapidapi.com/stock/get-histories?symbol=NBEV&from=1546448400&to=1562086800&events=div&interval=1d&region=US"))
				.header("x-rapidapi-key", "86282bc735msh9b714e4497955d5p1164fbjsnb690becc367a")
				.header("x-rapidapi-host", "apidojo-yahoo-finance-v1.p.rapidapi.com")
				.method("GET", HttpRequest.BodyPublishers.noBody())
				.build();
		HttpResponse<String> response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
		System.out.println(response.body());
	}
}
