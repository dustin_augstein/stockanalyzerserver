package com.stockanalyzer.service.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class EntryModel extends AbstractModel
{
	private String timestamp;
	private String ticker;
	private String tickerHref;
	private String reddit;
	private String prevScore;
	private String scoreChange;
	private String rockets;
	private String stockPrice;
	private String prevClose;
	private String change;
	private String shorted;
	private String volume;
	
	private String type;
	private String interval;

	public EntryModel()
	{
		super();
	}

	public EntryModel(String id)
	{
		super(id);
	}
}
