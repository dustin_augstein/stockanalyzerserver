package com.stockanalyzer.service.model;

import lombok.Data;

@Data
public abstract class AbstractModel
{
	String id;

	public AbstractModel()
	{
	}

	public AbstractModel(String id)
	{
		this.id = id;
	}
}
