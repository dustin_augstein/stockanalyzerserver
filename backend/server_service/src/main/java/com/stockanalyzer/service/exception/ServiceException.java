package com.stockanalyzer.service.exception;

public class ServiceException extends RuntimeException
{
	private static final long serialVersionUID = 201903280926L;

	public ServiceException(String error)
	{
		super(error);
	}
}
